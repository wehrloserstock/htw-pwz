# Programmierwerkzeuge Präsentation

## Git History + Andere Git Sachen

### Git History 

`git log`



### Git Letzte Änderung

`bash` // Weil ZSH ^ erkennt

`git diff HEAD HEAD`

### Programmversionen auschecken 

`git branch`

`git checkout name`



Oder

`git checkout commit sha`

Oder

`git checkout HEAD~1`

dann 

`git switch -`



## Einchecken einer neuen Version

`git add & git commit`



## Kommandozeile

### Code Dokumentation Generieren

`mvn javadoc:javadoc`

Finden in `target/site/apidocs/index.html`



### Build mit maven

`mvn compile` und

Finden in `target/classes`



### Ausführen des Programms

`mvn package`

`./runproject.sh`



### Ausführen von Tests

`mvn test`



### Statische Analyse des Quellcodes aufgrund einer Software-Metrik

- Coverage
- `mvn jacoco:report`
- finden in `/target/site/jacoco/index.html`



- Checkstyle
- `mvn checkstyle:check` oder `mvn:site`



### Erstellen eines Installationspaketes

`mvn package`



### Clean

`mvn clean`





## Starten einer IDE

`idea .`



### Testen

1. Rechts maven -> Test



### Debuggen

- Debug von Main Run Config (oben Rechts)





## UML Generieren

Im IDE oder per `mvn plantuml-generator:generate`













