#include <stdio.h>
#include "com_htw_hello_Hello.h"

JNIEXPORT jstring JNICALL Java_com_htw_hello_Hello_sayHello(JNIEnv *env, jobject thisObj)
{
	jstring jstr = (*env)->NewStringUTF(env, "Hello World !");
	return jstr;
}
