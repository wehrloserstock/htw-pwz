@echo off
clear

$(jenv local 1.8)
JAVAROOT="$JAVA_HOME"

echo TESTING
echo building java
javac Hello.java

echo generating header from class file
javac -h .  Hello.java

echo compiling c file
gcc -fPIC -I $JAVAROOT/include -I $JAVAROOT/include/darwin -c Hello.c

echo linking dynamic link library
gcc -shared -o libHello.so Hello.o

echo testing

javac -cp .:./junit-platform-console-standalone-1.3.1.jar Tester.java
java -Djava.library.path=. -jar junit-platform-console-standalone-1.3.1.jar --scan-class-path --class-path=.

echo deleting temporary files
rm Hello.h Hello.o libHello.so Hello.class Tester.class
