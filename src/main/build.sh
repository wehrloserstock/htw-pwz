#!/bin/bash

$(jenv local 1.8) #need this because of jenv
JAVAROOT="$JAVA_HOME"
echo $JAVAROOT
echo building java
javac Hello.java

echo generating header from class file
javac -h .  Hello.java

echo compiling c file
gcc -fPIC -I $JAVAROOT/include -I $JAVAROOT/include/darwin -c Hello.c

echo linking dynamic link library
gcc -shared -o libHello.so Hello.o

libpath=$(pwd)

echo running Java programm


java Hello

echo deleting temporary files
#rm com.htw.hello.Hello.h com.htw.hello.Hello.o libHello.so com.htw.hello.Hello.class
