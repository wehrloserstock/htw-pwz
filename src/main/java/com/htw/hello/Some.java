package com.htw.hello;

/**
 * This is some javadoc comment.
 */
public final class Some implements ISome {
    /**
     * This is some javadoc comment.
     */
    public Some() {

    }

    /**
     * This is some javadoc comment of an overridden method.
     */
    @Override
    public void printSomething() {
        System.out.println("This is something");
    }
}
