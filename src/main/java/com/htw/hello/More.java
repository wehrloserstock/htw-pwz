package com.htw.hello;

/**
 * This is some javadoc comment.
 */
public final class More {
    /**
     * This is also a  javadoc comment.
     */
    public void printSomethingElse() {
        System.out.println("This is something else");
    }
}
