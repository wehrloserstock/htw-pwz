package com.htw.hello;

/**
 * This is just for the command line calling part.
 */
final class Main {

    /**
     * Private Constructor.
     */
    private Main() {

    }

    /**
     * Main function.
     * @param args : Unneeded here.
     */
    public static void main(final String[] args) {
        String str = new Hello().sayHello();
        System.out.println(str);
    }
}
