package com.htw.hello;

/**
 * <p>com.htw.hello.Hello class.</p>
 *
 * @author julian.krieger
 * @version $Id: $Id
 */
public class Hello {

    static {
        System.loadLibrary("Hello");
    }

    /**
     * <p>sayHello.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public native String sayHello();

    /**
     * Example JAVADOC.
     */
    public void callHello() {
        this.sayHello();
    }
}
