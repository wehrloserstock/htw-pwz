package com.htw.hello;

/**
 * This is a test class.
 */
public final class Classes {

    /**
     * This is a test method.
     */
    public void printSomethingElseEntirely() {
        System.out.println("This is something else entirely");
    }
}
