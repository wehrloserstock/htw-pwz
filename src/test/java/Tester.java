import com.htw.hello.Classes;
import com.htw.hello.Hello;
import com.htw.hello.More;
import com.htw.hello.Some;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Tester {

	@Test
	void testCase() {
		System.out.println("Loading Library...");
		String javaLibPath = System.getProperty("java.library.path");
		System.loadLibrary("Hello");
		assertEquals(new Hello().sayHello(), "Hello World !");
	}

	@Test
	void testSome(){
		new Some().printSomething();
		assert(true);
	}

	@Test
	void testMore(){
		new More().printSomethingElse();
		assert(true);
	}

	@Test
	void testClasses(){
		new Classes().printSomethingElseEntirely();
		assert(true);
	}


}
